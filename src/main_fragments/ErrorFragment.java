package main_fragments;

import se.tuxflux.assignment.one.R;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Debug fragment, ignore.
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 * 
 */

public class ErrorFragment extends Fragment{
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		return inflater.inflate(R.layout.frag_error, null);
	}
}
