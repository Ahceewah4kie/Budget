package main_fragments;

import java.util.Calendar;
import java.util.GregorianCalendar;

import objects.Income;
import others.ContextPasser;
import others.IntentChecker;
import se.tuxflux.assignment.one.R;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;
import arrayadapters.IncomeAdapter;
import controllers.BarcodeController;
import controllers.IncomeController;
import dialogs.EditTransactionPopup;
import dialogs.ExpensePopup;
import dialogs.IncomePopup;

/**
 * A fragment that lets the user see their incomes and manipulate them.
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 * 
 */

public class IncomeFragment extends Fragment {

	private final String INCOME_TAG = "addincometag";
	private final String EXPENCE_TAG = "addexpencetag";
	private final String EDITTRANSACTION_TAG = "transactiontag";
	private final int BARCODE_SCANNER_RESULT = 1212;
	private final int INCOMEPOPUP_RESULT = 2001;
	private final int EDITTRANSACTION_RESULT = 1600;
	private Context context = ContextPasser.getLocalContext();
	private IncomeController controller = new IncomeController();
	private View view;
	private ListView listview;
	private Calendar cal = Calendar.getInstance();
	private IncomeAdapter adapter;
	private Spinner spToYear, spToMonth, spToDay, spFromYear, spFromMonth,
			spFromDay;
	Button btnSortEntries;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		Log.i("Budget", "Fragment income_fragment initiated!");
		view = inflater.inflate(R.layout.frag_income, null);
		setHasOptionsMenu(true);
		return view;
	}

	@Override
	public void onResume() {
		super.onResume();
		initComponents(view);
		initSpinners();
		setComponents();
		setHasOptionsMenu(true);
		
		btnSortEntries.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				getEntries();
			}
		});

		listview.setOnItemLongClickListener(new OnItemLongClickListener() {

			public boolean onItemLongClick(AdapterView<?> arg0, View arg1,
					int pos, long id) {
				EditTransactionPopup etp = new EditTransactionPopup();
				Income income = (Income) listview.getItemAtPosition(pos);
				Bundle data = new Bundle();

				String title = income.getTitle();
				String category = income.getCategory();
				double amount = income.getAmount();
				int incomeId = income.getId();

				data.putString("title", title);
				data.putString("category", category);
				data.putString("type", "income");
				data.putDouble("amount", amount);
				data.putInt("transaction_id", incomeId);

				etp.setArguments(data);
				etp.show(getFragmentManager(), EDITTRANSACTION_TAG);
				return true;
			}
		});
	}
	
	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.clear();
		inflater.inflate(R.menu.addtransaction, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_addExpence:
			ExpensePopup ep = new ExpensePopup();
			Bundle bundle = new Bundle();
			bundle.putString("eanCode", "");
			ep.setArguments(bundle);
			ep.show(getFragmentManager(), EXPENCE_TAG);
			return true;
		case R.id.action_addIncome:
			IncomePopup ip = new IncomePopup();
			ip.show(getFragmentManager(), INCOME_TAG);
			return true;
		case R.id.action_barcode:
			boolean isIntentAvailable = IntentChecker.isIntentAvailable(context,
			        "com.google.zxing.client.android.SCAN");
			if (isIntentAvailable) {
				Intent intent = new Intent("com.google.zxing.client.android.SCAN");
				intent.putExtra("SCAN_MODE", "PRODUCT_MODE");
				intent.putExtra("RESULT_DISPLAY_DURATION_MS", 0L);
				startActivityForResult(intent, 1212);
			}else {
				startActivity(new Intent(Intent.ACTION_VIEW,Uri.parse("market://details?id=com.google.zxing.client.android")));
					Toast.makeText(context, getString(R.string.zxingismissing), Toast.LENGTH_LONG).show();
			}
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent intent) {
		if (requestCode == BARCODE_SCANNER_RESULT && resultCode == Activity.RESULT_OK) {
			BarcodeController bcController = new BarcodeController();
			String eanCode = intent.getStringExtra("SCAN_RESULT");
			boolean isKnown = bcController.isKnownEan(eanCode);

			if (isKnown) {
				bcController.knownEan(eanCode);
				Toast.makeText(getActivity(), R.string.knowneancodeadded,
						Toast.LENGTH_SHORT).show();
			} else {
				Toast.makeText(getActivity(), R.string.unknowneancode,
						Toast.LENGTH_SHORT).show();
				ExpensePopup ep = new ExpensePopup();
				Bundle bundle = new Bundle();
				bundle.putString("eanCode", eanCode);
				ep.setArguments(bundle);
				ep.show(getFragmentManager(), EXPENCE_TAG);
			}
		}else if (requestCode == INCOMEPOPUP_RESULT) {
			addEntry();
		}else if (requestCode == EDITTRANSACTION_RESULT && resultCode == 1) {
			int id = intent.getIntExtra("id", -1);
			removeEntry(id);
		}else if (requestCode == EDITTRANSACTION_RESULT && resultCode == 2) {
			int id = intent.getIntExtra("id", -1);
			removeEntry(id);
			addEntry();
		}
	}
	
	/**
	 * Initilizes the components.
	 */
	private void initComponents(View view) {
		controller = new IncomeController();
		listview = (ListView) view.findViewById(R.id.listview_incomes);
		listview.setLongClickable(true);
		spToDay = (Spinner) view.findViewById(R.id.spinner_income_to_day);
		spToMonth = (Spinner) view.findViewById(R.id.spinner_income_to_month);
		spToYear = (Spinner) view.findViewById(R.id.spinner_income_to_year);
		spFromDay = (Spinner) view.findViewById(R.id.spinner_income_from_day);
		spFromMonth = (Spinner) view
				.findViewById(R.id.spinner_income_from_month);
		spFromYear = (Spinner) view.findViewById(R.id.spinner_income_from_year);
		btnSortEntries = (Button) view
				.findViewById(R.id.btn_income_submittimespan);
	}
	
	/**
	 * Sets values to the components.
	 */
	private void setComponents() {
		int fromYear = Integer.valueOf(spFromYear.getSelectedItem().toString());
		int fromMonth = spFromMonth.getSelectedItemPosition();
		int fromDay = spFromDay.getSelectedItemPosition() + 1;

		int toYear = Integer.valueOf(spToYear.getSelectedItem().toString());
		int toMonth = spToMonth.getSelectedItemPosition();
		int toDay = spToDay.getSelectedItemPosition() + 2;

		long startDate = new GregorianCalendar(fromYear, fromMonth, fromDay)
				.getTimeInMillis();
		long endDate = new GregorianCalendar(toYear, toMonth, toDay)
				.getTimeInMillis();

		adapter = new IncomeAdapter(context, R.layout.row_income,
				controller.getIncomes(startDate, endDate));
		listview.setAdapter(adapter);
	}
	
	/**
	 * Initilizes the spinners and fills them with data.
	 */
	private void initSpinners() {
		ArrayAdapter<Integer> adapter = new ArrayAdapter<Integer>(context,
				android.R.layout.simple_spinner_dropdown_item);
		int month = cal.get(cal.MONTH);
		int year = cal.get(cal.YEAR);
		int days; 

		for (int j = 0; j <= 31; j++) {  // TODO: Fix this date thingy. It should adjust "31" to days in each month.
			adapter.add(j);
		}

		days = cal.get(cal.DAY_OF_MONTH);
		year = (year - 2000);
		spFromDay.setAdapter(adapter);
		spToDay.setAdapter(adapter);
		spFromDay.setSelection(days);
		spToDay.setSelection(days);
		spFromMonth.setSelection(month - 1);
		spToMonth.setSelection(month);
		spFromYear.setSelection(year);
		spToYear.setSelection(year);
	}
	
	/**
	 * Shows the entries between two dates in the listview.
	 */
	private void getEntries() {
		int fromYear = Integer.valueOf(spFromYear.getSelectedItem().toString());
		int fromMonth = spFromMonth.getSelectedItemPosition();
		int fromDay = spFromDay.getSelectedItemPosition();
		int toYear = Integer.valueOf(spToYear.getSelectedItem().toString());
		int toMonth = spToMonth.getSelectedItemPosition();
		int toDay = spToDay.getSelectedItemPosition() + 1; // 2, since it's

		long startDate = new GregorianCalendar(fromYear, fromMonth, fromDay)
				.getTimeInMillis();
		long endDate = new GregorianCalendar(toYear, toMonth, toDay)
				.getTimeInMillis();

		adapter.clear();
		adapter.addAll(controller.getIncomes(startDate, endDate));
		adapter.notifyDataSetChanged();
	}

	/**
	 * Adds an entry to the listview.
	 */
	private void addEntry() {
		Income income = controller.getLatestIncome();
		adapter.add(income);
		adapter.notifyDataSetChanged();
	}

	/**
	 * Removes an entry from the listview.
	 */
	private void removeEntry(int id) {
		if (id != -1) {
			for (int i = 0; i < adapter.getCount(); i++) {
				if (adapter.getItem(i).getId() == id) {
					Income income = adapter.getItem(i);
					adapter.remove(income);
				}
			}
			adapter.notifyDataSetChanged();
		}else {
			Toast.makeText(context, getText(R.string.error_generic), Toast.LENGTH_SHORT).show();
		}
	}
}