package main_fragments;

import others.ContextPasser;
import others.UserHandler;
import se.tuxflux.assignment.one.R;
import android.app.ActionBar;
import android.app.ActionBar.OnNavigationListener;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ArrayAdapter;
import android.widget.SpinnerAdapter;
import android.widget.Toast;
import controllers.ExpensePopupController;
import controllers.IncomePopupController;
import dialogs.ExpensePopup;
import dialogs.FirstRunPopup;
import dialogs.GoalPopup;
import dialogs.IncomePopup;

public class MainActivity extends Activity {

	private final String LOGPREFIX = "Budget | Mainactivty";
	private final String INCOME_TAG = "addincometag";
	private final String EXPENCE_TAG = "addexpencetag";
	private final String SUBMITNAME_TAG = "submitnametag";
	private final String GOAL_TAG = "setgoaltag";
	private final String KEY_NEW_USER = "key_newuser";
	private final String KEY_HASNAME = "key_hasname";
	private final String KEY_FIRSTNAME = "key_firstname";
	private final String KEY_SUBNAME = "key_subname";
	private final String SP_USER = "sp_name";
	private Fragment fragment;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		if (newUser()) {
			submitName();
		}
		
		ActionBar ab = getActionBar();
		ab.setDisplayShowTitleEnabled(false); // Removes title from actionbar
		ab.setDisplayShowHomeEnabled(false); // Removes app-icon from actionbar
		super.onCreate(savedInstanceState);
		SpinnerAdapter spinnerAdapter = ArrayAdapter.createFromResource(this,
				R.array.menulist, R.layout.custom_spinner);
		OnNavigationListener navigationListener = new OnNavigationListener() {
			@Override
			public boolean onNavigationItemSelected(int itemPosition,
					long itemId) {
				switch (itemPosition) {
				case 0:
					switchFragment(new SummaryFragment());
					break;
				case 1:
					switchFragment(new ExpenseFragment());
					break;
				case 2:
					switchFragment(new IncomeFragment());
					break;
				case 3:
					switchFragment(new GoalFragment());
					break;
				default:
					switchFragment(new ErrorFragment());
					break;
				}
				return true;
			}
		};
		
		ab.setNavigationMode(ActionBar.NAVIGATION_MODE_LIST);
		setContentView(R.layout.activity_main);
		ContextPasser.setLocalContext(this);
		ab.setListNavigationCallbacks(spinnerAdapter, navigationListener);
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		UserHandler uh = new UserHandler();
		UserHandler.setStaticCurrency(uh.getCurrency());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.addtransaction, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
            return super.onOptionsItemSelected(item);
	}

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
	}

	/**
	 * Shows a dialog that lets the user submit its name.
	 */
	private void submitName() {
		FirstRunPopup firstRunPopup = new FirstRunPopup();
		firstRunPopup.show(getFragmentManager(), SUBMITNAME_TAG);
	}

	/**
	 * Checks if user has a name or not.
	 * @return true if user has a name.
	 */
	private boolean newUser() {
		SharedPreferences sp = getSharedPreferences(SP_USER,Context.MODE_PRIVATE);
		boolean newUser = sp.getBoolean(KEY_NEW_USER, true);
		return newUser;
	}

	/**
	 * Switches fragment. 
	 * @param fragment - the fragment to switch to.
	 */
	private void switchFragment(Fragment fragment) {
		FragmentManager fm = getFragmentManager();
		FragmentTransaction fragTrans = fm.beginTransaction();
		fragTrans.replace(R.id.fragment_container, fragment);
		fragTrans.addToBackStack(null);
		fragTrans.commit();
	}
}