package main_fragments;

import se.tuxflux.assignment.one.R;
import android.app.Fragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Debug fragment, ignore.
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 * 
 */

public class DebugFragment extends Fragment{
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
		Log.i("Budget", "Fragment goal_fragment initiated!");
		return inflater.inflate(R.layout.frag_income, null);
	}
}
