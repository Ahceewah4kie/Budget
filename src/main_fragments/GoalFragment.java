package main_fragments;

import others.ContextPasser;
import others.GoalHandler;
import others.UserHandler;
import se.tuxflux.assignment.one.R;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import database.DBAdapter;
import dialogs.GoalPopup;

/**
 * A fragment that lets the user see their goal.
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 * 
 */

public class GoalFragment extends Fragment {
	private GoalHandler goalHandler;
	private DBAdapter db;
	private UserHandler namehandler;
	private Context context = ContextPasser.getLocalContext();
	private static String name;
	private final String GOAL_TAG = "setgoaltag";
	private View view;
	private TextView tv_goal_object_title,tv_goal_price_title, tv_goal_object,tv_goal_price;
	private ProgressBar bar;
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {

		view = inflater.inflate(R.layout.frag_goal, null);
		
		return view;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		goalHandler = new GoalHandler();
		namehandler = new UserHandler();
		db = new DBAdapter(context);
		name = namehandler.getFullName();
		if (goalHandler.getGoalTitle() == "" || goalHandler.getGoalAmount() == "") {
			GoalPopup gp = new GoalPopup();
			gp.show(getFragmentManager(), GOAL_TAG);
			initComponents();
		} else {
			initComponents();
			setComponents();
		}
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.clear();
		inflater.inflate(R.menu.goal, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_setGoal:
			GoalPopup gp = new GoalPopup();
			gp.show(getFragmentManager(), GOAL_TAG);
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 1002) {
			setComponents();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	/**
	 * Initilizes the components.
	 */
	private void initComponents() {
		setHasOptionsMenu(true);
		tv_goal_object_title = (TextView)view.findViewById(R.id.tv_goal_object_title);
		tv_goal_price_title = (TextView) view
		.findViewById(R.id.tv_goal_price_title);
		tv_goal_object = (TextView) view
				.findViewById(R.id.tv_goal_object);
		tv_goal_price = (TextView) view
				.findViewById(R.id.tv_goal_objects_price);
		bar = (ProgressBar) view
				.findViewById(R.id.progressbar_goal);
		tv_goal_object_title.setVisibility(View.GONE);
		tv_goal_price_title.setVisibility(View.GONE);
		tv_goal_object.setVisibility(View.GONE);
		tv_goal_price.setVisibility(View.GONE);
		bar.setVisibility(View.GONE);
	}

	/**
	 * Gets the current balance from db.
	 * @return the current balance. TODO: Should be a double..?
	 */
	private int getCurrentBalance() {
		int shortage = 0;
		db.open();
		Cursor expense = db.getExpenseSumCursor();
		Cursor income = db.getIncomeSumCursor();
		if (expense.getCount() > 0 && income.getCount() > 0) {
			income.moveToNext();
			expense.moveToNext();
			shortage = income.getInt(0);
			shortage -= expense.getInt(0);
		}
		return shortage;
	}
	
	/**
	 * Sets values to the components.
	 */
	private void setComponents() {
		double goal = Double.parseDouble(goalHandler.getGoalAmount());
		int balance = getCurrentBalance();
		double result = (balance - goal);
		
		if (balance > 0 && result >0 ){
			String title = getString(R.string.exceededgoal);
			tv_goal_price_title.setText(title);
			bar.setProgress((int)100);
		}else if (balance > 0) {
			bar.setMax((int) goal);
			bar.setProgress((int) balance);
		}else {
			bar.setVisibility(View.GONE);
		}
		tv_goal_object_title.setText(namehandler.getFullName()+", "+getString(R.string.yourgoalis));
		tv_goal_object.setText(goalHandler.getGoalTitle());
		tv_goal_price.setText(String.valueOf(result) + namehandler.getCurrency());
		
		tv_goal_object_title.setVisibility(View.VISIBLE);
		tv_goal_price_title.setVisibility(View.VISIBLE);
		tv_goal_object.setVisibility(View.VISIBLE);
		tv_goal_price.setVisibility(View.VISIBLE);
		bar.setVisibility(View.VISIBLE);
	}
}
