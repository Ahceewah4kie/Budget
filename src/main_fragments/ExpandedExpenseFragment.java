package main_fragments;

import java.io.File;
import java.util.Calendar;

import others.ContextPasser;
import se.tuxflux.assignment.one.R;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;
import controllers.ExpensePopupController;
import others.ContextPasser;
import se.tuxflux.assignment.one.R;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Point;
import android.os.Environment;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Toast;
import database.DBAdapter;


/**
 * A fragment that lets the user add an expense with a photo.
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 * 
 */

public class ExpandedExpenseFragment extends Fragment {
	private Fragment fragment;
	private Context context = ContextPasser.getLocalContext();
	private ExpensePopupController controller = new ExpensePopupController();
	private ActionBar ab;
	private View view;
	private Calendar cal = Calendar.getInstance();
	private ImageButton btnDate;
	private Bitmap image;
	private String title,category,price;
	private File filepath;

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		view = inflater.inflate(R.layout.expanded_dialog_addexpence, null);
		return view;
	}
	
	@Override
	public void onResume() {
		super.onResume();
		ab = getActivity().getActionBar();
		setHasOptionsMenu(true);
		ImageView iv_camera = (ImageView) view.findViewById(R.id.iv_expanded_expense_image);

		iv_camera.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v) {
				try {
					Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
					filepath = controller.createImageFile();
					takePictureIntent.putExtra(MediaStore.EXTRA_OUTPUT, Uri.fromFile(filepath));
					startActivityForResult(takePictureIntent, 100);
				} catch (Exception e) {
					Toast.makeText(context,
                            context.getText(R.string.error_savingimage),
                            Toast.LENGTH_LONG).show();
				}
			}
		});
	}

	@Override
	public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
		menu.clear();
		inflater.inflate(R.menu.submit_menu, menu);
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_submit:
			if (getInput()) {
				controller.submitPressed(title, category, price, this.image);
				image = null;
			}else {
				Toast.makeText(getActivity(), R.string.error_notdouble, Toast.LENGTH_SHORT).show();
			}
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
		if (requestCode == 100 && resultCode != 0) {
			onPictureTaken();
		}
		super.onActivityResult(requestCode, resultCode, data);
	}
	
	/**
	 * Is called from onActivityResult when the picture has been taken.
	 * Calls the controller to resize it, the shows the picture to the user.
	 */
	private void onPictureTaken() {
		ImageView iv_image = (ImageView) view.findViewById(
                R.id.iv_expanded_expense_image);
		Bitmap image = BitmapFactory.decodeFile(filepath.toString());
		image = controller.resizeImageToFitScreenSize(image);
        iv_image.setPadding(0, 0, 0, 0);
		iv_image.setImageBitmap(image);
		this.image = image;
	}
	
	/**
	 * Gets input from edittext if they are not empty.
	 * @return true if input from edittext if they are not empty.
	 */
	private boolean getInput() {
		EditText et_title = (EditText) view
				.findViewById(R.id.et_expanded_expense_title);
		EditText et_category = (EditText) view
				.findViewById(R.id.et_expanded_expense_category);
		EditText et_price = (EditText) view
				.findViewById(R.id.et_expanded_expense_price);
		boolean isFilled = true;

		title = et_title.getText().toString();
		category = et_category.getText().toString();
		price = et_price.getText().toString();
		
		if (title.isEmpty()) {
			isFilled = false;
		}else if (category.isEmpty()) {
			isFilled = false;
		}else if (price.isEmpty()) {
			isFilled = false;
		}
		return isFilled;
	}
}