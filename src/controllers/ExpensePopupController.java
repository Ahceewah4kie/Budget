package controllers;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import others.ContextPasser;
import se.tuxflux.assignment.one.R;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Point;
import android.os.Environment;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;
import android.widget.Toast;
import database.DBAdapter;
import others.ContextPasser;
import se.tuxflux.assignment.one.R;
import android.app.ActionBar;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;
import controllers.ExpensePopupController;

/**
 * A controller, controlling expenses and image related methods.
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 */

public class ExpensePopupController {
	private static final String LOGPREFIX = "Budget | ExpenseController";
	private Context context = ContextPasser.getLocalContext();
	private DBAdapter db = new DBAdapter(context);
	private Calendar cal = Calendar.getInstance();
	private static final String SP_IMAGENUMBER = "sharedimagenumber";
	private static final String GET_SP_IMAGENUMBER = "getimagenumber";

	/**
	 * Adds a new expense row to the db, with all but a image.
	 * 
	 * Note that the price is a string. Since the user is only allowed to use
	 * digits and a "," or "." (depending on locale), this won't be a problem.
	 * 
	 * @param title
	 *            - title of expense
	 * @param category
	 *            - category of expense
	 * @param priceString
	 *            - price of expense
	 */
	public void addQuickExpense(String title, String category,
			String priceString, long timestamp, String ean) {
		try {
			double amount = Double.valueOf(priceString);
			db.open();
			db.addExpense(title, category, amount, timestamp, ean);
			db.close();
			Toast.makeText(context,
					context.getString(R.string.addedexpense) + ": " + title,
					Toast.LENGTH_SHORT).show();
		} catch (Exception e) {
			Toast.makeText(context, context.getText(R.string.error_notdouble),
					Toast.LENGTH_SHORT).show();
		}
	}

	/**
	 * Adds a new expense row to the db.
	 * 
	 * Note that the price is a string. Since the user is only allowed to use
	 * digits and a "," or "." (depending on locale), this won't be a problem.
	 * 
	 * @param title
	 *            - title of expense
	 * @param category
	 *            - category of expense
	 * @param priceString
	 *            - price of expense
	 * @param image
	 *            - a byte[] that represent a image
	 */
	public void addFullExpense(String title, String category, String priceString, Bitmap image, long timestamp) {
		double amount = Double.valueOf(priceString);
		image = resizeImageToThumb(image); // Shrinks image to
											// thumbnail size.
		byte[] imageArray = imageToArray(image); // Encodes image to a
													// byte[].
		db.open();
		db.addExpense(title, category, amount, imageArray, timestamp, "");
		db.close();
		Toast.makeText(context,
				context.getString(R.string.addedexpense) + ": " + title,
				Toast.LENGTH_SHORT).show();
	}

	/**
	 * Encodes a Bitmap as a byte[], so it can be written to db.
	 * 
	 * @param image
	 *            - bitmap to encode.
	 * @return byte[], representing a bitmap in .JPEG format.
	 */
	private byte[] imageToArray(Bitmap image) {
		ByteArrayOutputStream bos = new ByteArrayOutputStream();
		image.compress(CompressFormat.JPEG, 0, bos);
		byte[] imageArray = bos.toByteArray();
		StringBuffer output = new StringBuffer();
		for (int i = 0; i < imageArray.length; i++) {
			output.append(imageArray[i]);
		}
		Log.i(LOGPREFIX, "byte[]=" + output.toString());
		return imageArray;
	}

	/**
	 * Rezises a bitmap, but keeps its aspect ratio.
	 * @param image - the bitmap to resize.
	 * @return the resized bitmap.
	 */
	private Bitmap resizeImageToThumb(Bitmap image) {
		double newWidth = image.getWidth();
		double newHeight = image.getHeight();
		
		// Lower numbers here, equals lower resolution - ie. lower RAM usage!
		if (newHeight > newWidth) {
			// Portrait
			double ratio = newHeight / newWidth;
			newHeight = 200 * ratio; 
			newWidth = 200; 

		} else if (newHeight < newWidth) {
			// Landscape
			double ratio = newWidth / newHeight;
			newHeight = 200;
			newWidth = 200 * ratio;
		}
		return Bitmap.createScaledBitmap(image, (int) newWidth,
				(int) newHeight, true);
	}

	/**
	 * Method that is called when the user presses on submit. Calls either addFullExpense or addQuickExpense to add input to database.
	 * @param title - expense title.
	 * @param category - expense category.
	 * @param price - expense price.
	 * @param image - expense image. If null, addQuickExpense will be used instead of addFullExpense.
	 */
	public void submitPressed(String title, String category, String price, Bitmap image) {
		long date = cal.getTimeInMillis();
		if (image != null) {
			addFullExpense(title, category, price, image, date);
		}else {
			addQuickExpense(title, category, price, date, "");
		}
	}

	/**
	 * Creates a container file for a picture to be saved in.
	 * @return a File, containing the path to a container file.
	 * @throws IOException if dir is not accesseble.
	 */
	public File createImageFile() throws IOException {
		File imageFile = File
				.createTempFile(
						getImageName(),
						".JPEG",
						Environment
								.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES));
		Log.i(LOGPREFIX, imageFile.toString());
		return imageFile;
	}

	/**
	 * Gets a uniqe name for the image.
	 * @return a String containing a name, based on time. yyyyMMdd_HHmmss.
	 */
	private String getImageName() {
		Calendar cal = Calendar.getInstance();
		SharedPreferences sp = context.getSharedPreferences(SP_IMAGENUMBER,
				context.MODE_PRIVATE);
		Editor edit = sp.edit();
		String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").toString(); //TODO: Check locale!
		edit.putString(GET_SP_IMAGENUMBER, timeStamp);
		edit.apply();
		return "budget_picture_" + timeStamp;
	}

	/**
	 * Resized a picture to fit the screen, so it could be showed as a preview.
	 * @param image - the image, as a bitmap.
	 * @return the resized bitmap.
	 */
	public Bitmap resizeImageToFitScreenSize(Bitmap image) {
		// ----------Stolen code--------------
		// http://stackoverflow.com/questions/1016896/how-to-get-screen-dimensions
		WindowManager wm = (WindowManager) context
				.getSystemService(Context.WINDOW_SERVICE);
		Display display = wm.getDefaultDisplay();
		Point size = new Point();
		display.getSize(size);
		int screenWidth = size.x;
		int screenHeight = size.y;
		// ------------------------------------

		int newWidth = image.getWidth();
		int newHeight = image.getHeight();

		newHeight = (int) (newHeight / 5); // TODO: Change to something more
											// dynamically!
		newWidth = (int) (newWidth / 5); // TODO: Change to something more
											// dynamically!
		return Bitmap.createScaledBitmap(image, newWidth, newHeight, true);
	}
}