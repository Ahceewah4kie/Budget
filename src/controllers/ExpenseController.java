package controllers;

import java.util.ArrayList;

import objects.Expense;
import others.ContextPasser;
import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import database.DBAdapter;

/**
 * A controller, controlling expenses.
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 */
public class ExpenseController {
	private static final String LOGPREFIX = "Budget | ExpenseController";
	private Context context = ContextPasser.getLocalContext();
	private DBAdapter db = new DBAdapter(context);

	/**
	 * Returns all expenses between the two dates that the database holds, as an ArrayList.
	 * @param startdate - the startdate, should be timeinmilliseconds() to be valid.
	 * @param enddate - the enddate, should be timeinmilliseconds() to be valid.
	 * @return An ArrayList<Expense>, holding all expenses between the two dates that the database holds.
	 */
	public ArrayList<Expense> getExpenses(long startdate, long enddate) {
		ArrayList<Expense> expenses = new ArrayList<Expense>();
		Cursor c;
		db.open();
		c = db.getExpense(startdate, enddate);
		while (c.getCount() > 0 && !c.isLast()) {
			c.moveToNext();
			Expense e = new Expense();
			e.setTitle(c.getString(0));
			e.setCategory(c.getString(1));
			e.setAmount(c.getDouble(2));
			byte[] imageblob = c.getBlob(3);
			if (imageblob != null) {
				e.setImage(BitmapFactory.decodeByteArray(imageblob, 0,
						imageblob.length));
			}
			e.setId(c.getInt(4));
			e.setTimestamp(c.getLong(5));
			expenses.add(e);
		}
		db.close();
		return expenses;
	}
	
	/**
	 * Gets the last submitted expense from the database and returns it as an Expense object.
	 * @return the last submitted expense from the database as an Expense object.
	 */
	public Expense getLatestExpense() {
		Cursor c;
		Expense e = new Expense();
		db.open();
		c = db.getLatestExpense();
		if (c.moveToNext()) {
			e.setTitle(c.getString(0));
			e.setCategory(c.getString(1));
			e.setAmount(c.getDouble(2));
			byte[] imageblob = c.getBlob(3);
			if (imageblob != null) {
				e.setImage(BitmapFactory.decodeByteArray(imageblob, 0,
						imageblob.length));
			}
			e.setId(c.getInt(4));
			e.setTimestamp(c.getLong(5));
		}
		db.close();
		return e;
	}
}
