package controllers;

import java.util.ArrayList;
import java.util.Calendar;

import objects.Expense;
import objects.Income;
import others.ContextPasser;
import se.tuxflux.assignment.one.R;
import android.app.Fragment;
import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.view.View;
import android.widget.ListView;
import arrayadapters.IncomeAdapter;
import database.DBAdapter;

/**
 * A controller, controlling incomes.
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 */

public class IncomeController {

	private IncomeAdapter adapter;
	private static final String LOGPREFIX = "Budget | ExpenseController";
	private Context context = ContextPasser.getLocalContext();
	private DBAdapter db = new DBAdapter(context);
	private static Fragment fragment;
	private static Calendar cal = Calendar.getInstance();

	public IncomeController() {
		// TODO Auto-generated constructor stub
	}
	
	public IncomeController(Fragment fragment) {
		this.fragment = fragment;
	}
	
	/**
	 * Returns all incomes between the two dates that the database holds, as an ArrayList.
	 * @param startdate - the startdate, should be timeinmilliseconds() to be valid.
	 * @param enddate - the enddate, should be timeinmilliseconds() to be valid.
	 * @return An ArrayList<Incomes>, holding all incomes between the two dates that the database holds.
	 */
	public ArrayList<Income> getIncomes(long startdate, long enddate) {
		ArrayList<Income> incomes = new ArrayList<Income>();
		Cursor c;
		db.open();
		c = db.getIncome(startdate, enddate);
			while (c.getCount() > 0 && !c.isLast()) {
				c.moveToNext();
				Income i = new Income();
				i.setTitle(c.getString(0));
				i.setCategory(c.getString(1));
				i.setAmount(c.getDouble(2));
				i.setId(c.getInt(3));
				i.setTimestamp((c.getLong(4)));
				incomes.add(i);
			}
			db.close();
		return incomes;
	}
	
	/**
	 * Gets the last submitted income from the database and returns it as a Income object.
	 * @return the last submitted income from the database as a Income object.
	 */
	public Income getLatestIncome() {
		Cursor c;
		Income i = new Income();
		db.open();
		c = db.getLatestIncome();
		if (c.moveToNext()) {
			i.setTitle(c.getString(0));
			i.setCategory(c.getString(1));
			i.setAmount(c.getDouble(2));
			i.setId(c.getInt(3));
			i.setTimestamp((c.getLong(4)));
		}
		db.close();
		return i;
	}
	
	/**
	 * Populates the lisview. TODO: Should be moved to the View class.
	 * @param view - the view that inflates the listview.
	 * @param startdate - the startdate, from wich incomes will be listed.
	 * @param enddate - the enddate, *until* wich incomes will be listed.
	 */
	public void populateListView(View view, int startdate, int enddate) {
		ListView listview = (ListView) view.findViewById(R.id.listview_incomes);
		adapter = new IncomeAdapter(context, R.layout.row_income, getIncomes(startdate, enddate));
		listview.setAdapter(adapter);
	}
}
