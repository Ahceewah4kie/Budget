package controllers;

import others.ContextPasser;
import android.content.Context;
import database.DBAdapter;
/**
 * A controller, controlling the transaction dialog.
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 */
public class EditTransactionController {
	private Context context = ContextPasser.getLocalContext();
	private DBAdapter db = new DBAdapter(context);
	private static String INCOME = "income", EXPENSE="expense";

	/**
	 * Asks the database to remove the specified entry.
	 * @param id - the id of the entry.
	 * @param type - this determines if the object is a income or an expense. Should be either "income" or "expense". Poor implementation.
	 */
	public void removeTransaction(int id, String type) {		
		db.open();
		if (type.equals(INCOME)) {
			db.removeIncome(id);
		}else if(type.equals(EXPENSE)) {
			db.removeExpense(id);		
		}
		db.close();
	}

	/**
	 * Asks the database to update the specified entry.
	 * @param id - the id of the entry.
	 * @param type - this determines if the object is a income or an expense. Should be either "income" or "expense". Poor implementation.
	 */
	public void editTransaction(String title, String category, double amount, int id, String type) {		
		db.open();
		if (type.equals(INCOME)) {
			db.editIncome(title, category, amount, id);
		}else if(type.equals(EXPENSE)) {
			db.editExpense(title, category, amount, id);
		}
		db.close();
	}
}
