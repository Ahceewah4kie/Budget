package controllers;

import java.util.Calendar;

import others.ContextPasser;
import android.content.Context;
import android.database.Cursor;
import database.DBAdapter;
/**
 * A controller class for barcode scanning.
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 */
public class BarcodeController {
	private Context context = ContextPasser.getLocalContext();
	private DBAdapter db = new DBAdapter(context);

	/**
	 * Searches the database for entries that has the ean number and returns a status.
	 * @param ean - the ean number that has been scanned.
	 * @return true if the ean nuber already exists in database, else false.
	 */
	public boolean isKnownEan(String ean) {
		db.open();
		boolean exists;
		Cursor cEan = db.getEan(ean);
		if (cEan.moveToNext()) {
			exists = true;
		} else {
			exists = false;
		}
		db.close();
		return exists;
	}

	/**
	 * If the ean number exists in database, add a new entry of the product.
	 * @param ean - the ean number as a String
	 */
	public void knownEan(String ean) {	
		db.open();
		Cursor cEan = db.getEan(ean);
		if(cEan.moveToNext()) {
			String title = cEan.getString(0);
			String category = cEan.getString(1);
			double price = cEan.getDouble(2);
			byte[] imageblob = cEan.getBlob(3);
			long timestamp = Calendar.getInstance().getTimeInMillis();
			try {
				db.addExpense(title, category, price, imageblob, timestamp, ean);
			} catch (Exception e) {
				db.addExpense(title, category, price, timestamp, ean);
			}
		}
		db.close();
	}
}
