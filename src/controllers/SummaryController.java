package controllers;

import java.util.ArrayList;

import objects.Expense;
import objects.Income;
import others.ContextPasser;
import android.content.Context;
import android.database.Cursor;
import database.DBAdapter;

/**
 * A controller, controlling the Summary View.
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 */
public class SummaryController {
	private DBAdapter db;
	private Context context = ContextPasser.getLocalContext();
	private int totalIncome = 0, totalExpense = 0;

	public SummaryController() {
		db = new DBAdapter(context);
	}

	/**
	 * Gets the total income from database and returns it as an int.
	 * @return the total sum of all incomes as an int. TODO: Should return a double, for more exact statistics.
	 */
	public int getTotalIncome() {
		db.open();
		Cursor cIncome = db.getIncomeSumCursor();
		if (cIncome.getCount() > 0) {
			cIncome.moveToNext();
			totalIncome = cIncome.getInt(0);
		}
		db.close();
		return totalIncome;
	}
	
	/**
	 * Gets the total expenses from database and returns it as an int.
	 * @return the total sum of all expenses as an int. TODO: Should return a double, for more exact statistics.
	 */
	public int getTotalExpense() {
		db.open();
		Cursor cExpense = db.getExpenseSumCursor();
		if (cExpense.getCount() > 0) {
			cExpense.moveToNext();
			totalExpense = cExpense.getInt(0);
		}
		db.close();
		return totalExpense;
	}

	/**
	 * Gets all incomes and returns it as an ArrayList<Income>.
	 * @return an ArrayList<Income>, holding all incomes the database has.
	 */
	public ArrayList<Income> getIncome() {
		ArrayList<Income> incomes = new ArrayList<Income>();
		db.open();
		Cursor cIncome = db.getAllIncome();
		while (cIncome.getCount() > 0 && !cIncome.isLast()) {
			cIncome.moveToNext();
			Income income = new Income();
			String title = cIncome.getString(0);
			String category = cIncome.getString(1);
			double amount = cIncome.getDouble(2);
			int id = cIncome.getInt(3);
			long timestamp = cIncome.getLong(4);

			income.setTitle(title);
			income.setCategory(category);
			income.setAmount(amount);
			income.setId(id);
			income.setTimestamp(timestamp);
			incomes.add(income);
		}
		db.close();
		return incomes;
	}
	
	/**
	 * Gets all expenses and returns it as an ArrayList<Expense>.
	 * @return an ArrayList<Expense>, holding all expenses the database has.
	 */
	public ArrayList<Expense> getExpense() {
		ArrayList<Expense> expenses = new ArrayList<Expense>();
		db.open();
		Cursor cExpense = db.getAllExpenses();
		while (cExpense.getCount() > 0 && !cExpense.isLast()) {
			cExpense.moveToNext();
			Expense expense = new Expense();
			String title = cExpense.getString(0);
			String category = cExpense.getString(1);
			double amount = cExpense.getDouble(2);
			int id = cExpense.getInt(4);
			long timestamp = cExpense.getLong(5);

			expense.setTitle(title);
			expense.setCategory(category);
			expense.setAmount(amount);
			expense.setId(id);
			expense.setTimestamp(timestamp);
			expenses.add(expense);
		}
		db.close();
		return expenses;
	}
}