package controllers;

import others.ContextPasser;
import android.app.Dialog;
import android.app.Fragment;
import android.content.Context;
import database.DBAdapter;

/**
 * A controller, controlling incomes in popup.
 * TODO: This class is redundant to IncomeController. Should be moved and removed.
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 */
public class IncomePopupController {
	private static final String LOGPREFIX = "Budget | ExpenseController";
	private Context context = ContextPasser.getLocalContext();
	private DBAdapter db = new DBAdapter(context);
	private Dialog dialogview;
	private Fragment parent;

	/**
	 * Asks the database to save a income.
	 * @param title - income title.
	 * @param category - income category.
	 * @param amount - income amount.
	 * @param timestamp - income timestamp. TODO: Should timestamp be added here..?
	 */
	public void addIncome(String title, String category, double amount, long timestamp) {
		db.open();
		db.addIncome(title, category, amount, timestamp);
		db.close();
	}
}
