package dialogs;

import java.util.Calendar;

import main_fragments.ExpandedExpenseFragment;
import main_fragments.ExpenseFragment;
import se.tuxflux.assignment.one.R;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;
import controllers.ExpensePopupController;

/**
 * Creates a dialog where users are allowed to add an expense.
 * 
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 * 
 */
public class ExpensePopup extends DialogFragment {

	private String title, category, price;
	private EditText etTitle, etCategory, etPrice;
	private Dialog dialogview;
	private ExpensePopupController controller;
	private ImageView iv_image;
	private String ean = "";
	private Calendar cal = Calendar.getInstance();

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflater = getActivity().getLayoutInflater();
		ean = getArguments().getString("eanCode");
		dialogview = getDialog();
		controller = new ExpensePopupController();
		builder.setView(inflater.inflate(R.layout.dialog_addexpence, null))

				.setNeutralButton(getString(R.string.additional_expense),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								getInput();
								FragmentManager fm = getFragmentManager();
								FragmentTransaction fragTrans = fm
										.beginTransaction();
								fragTrans.replace(R.id.fragment_container,
										new ExpandedExpenseFragment());
								fragTrans.commit();
							}
						})

				.setPositiveButton(getString(R.string.submit),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								getInput();
								String[] months = getResources()
										.getStringArray(R.array.month);
								long timestamp = Calendar.getInstance()
										.getTimeInMillis();
								controller.addQuickExpense(title, category,
										price, timestamp, ean);
								Fragment fragment = getFragmentManager()
										.findFragmentById(2131296256);
								fragment.onActivityResult(2000, 1, null);
							}
						});
		return builder.create();
	}

	/**
	 * Gets input from the EditText's.
	 */
	private void getInput() {
		dialogview = getDialog();
		etTitle = (EditText) dialogview.findViewById(R.id.et_expence_title);
		etCategory = (EditText) dialogview
				.findViewById(R.id.et_expence_category);
		etPrice = (EditText) dialogview.findViewById(R.id.et_expence_price);
		title = etTitle.getText().toString();
		category = etCategory.getText().toString();
		price = etPrice.getText().toString();
	}
}