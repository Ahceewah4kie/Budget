package dialogs;

import se.tuxflux.assignment.one.R;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;
import controllers.EditTransactionController;

/**
 * Creates a dialog where users are allowed to either remove or edit a listview entry.
 * 
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 * 
 */
public class EditTransactionPopup extends DialogFragment {

	private Dialog dialogview;
	private int transactionId;
	private String title, category, type;
	private double amount;
	private EditText etTitle, etCategory, etAmount;
	private EditTransactionController controller;

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflater = getActivity().getLayoutInflater();
		View view = inflater.inflate(R.layout.dialog_edittransaction, null);
		controller = new EditTransactionController();
		dialogview = getDialog();

		title = getArguments().getString("title");
		category = getArguments().getString("category");
		type = getArguments().getString("type");
		amount = getArguments().getDouble("amount");
		transactionId = getArguments().getInt("transaction_id");

		etTitle = (EditText) view.findViewById(R.id.et_edittransaction_title);
		etCategory = (EditText) view
				.findViewById(R.id.et_edittransaction_category);
		etAmount = (EditText) view.findViewById(R.id.et_edittransaction_amount);

		String amountString = String.valueOf(amount);
		etTitle.setText(title);
		etCategory.setText(category);
		etAmount.setText(amountString);

		builder.setView(view)

				.setNegativeButton(getString(R.string.cancel),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dismiss();
							}
						})
						
				.setNeutralButton(getString(R.string.remove),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								controller.removeTransaction(transactionId,
										type);
								notifyChange(1);
								Toast.makeText(getActivity(), getString(R.string.removedentry) + " " +title , Toast.LENGTH_SHORT).show();
							}
						})

				.setPositiveButton(getString(R.string.save),
						new DialogInterface.OnClickListener() {
							@Override
							public void onClick(DialogInterface dialog,
									int which) {
								dialogview = getDialog();
								etTitle = (EditText) dialogview
										.findViewById(R.id.et_edittransaction_title);
								etCategory = (EditText) dialogview
										.findViewById(R.id.et_edittransaction_category);
								etAmount = (EditText) dialogview
										.findViewById(R.id.et_edittransaction_amount);
								String t = etTitle.getText().toString();
								String c = etCategory.getText().toString();
								String a = etAmount.getText().toString();
								double aDouble = Double.valueOf(a);
								controller.editTransaction(t, c, aDouble,
										transactionId, type);
								notifyChange(2);
								Toast.makeText(getActivity(), getString(R.string.updatedentry) + " " +title , Toast.LENGTH_SHORT).show();
							}
						});
		return builder.create();
	}

	/**
	 * Returns the ID of an expense or income to the Viewer class the holds the listview.
	 * @param buttonID - 1 == remove, 2 == edit (save)
	 */
	private void notifyChange(int buttonID) {
		Fragment fragment = getFragmentManager().findFragmentById(2131296256);
		Intent data = new Intent();
		data.putExtra("id", transactionId);
		fragment.onActivityResult(1600, buttonID, data);
	}
}