package dialogs;

import others.ContextPasser;
import others.GoalHandler;
import se.tuxflux.assignment.one.R;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

/**
 * Creates a dialog where users are allowed to save a saving goal.
 * 
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 * 
 */
public class GoalPopup extends DialogFragment {

	private EditText et_title, et_amount;
	private String title, amount;
	private Dialog dialogview;
	private Context context;
	private GoalHandler controller;
	
	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflater = getActivity().getLayoutInflater();
		dialogview = getDialog();
		controller = new GoalHandler();
		context = ContextPasser.getLocalContext();
		builder.setView(inflater.inflate(R.layout.dialog_setgoal, null))
		.setNegativeButton(R.string.cancel, new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				dismiss();
			}
		})
		.setPositiveButton(getString(R.string.submit), new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				//Overridden
			}
		});
		AlertDialog dialog = builder.create();
		dialog.show();
		dialog.getButton(AlertDialog.BUTTON_POSITIVE).setOnClickListener(
				new View.OnClickListener() {
					@Override
					public void onClick(View v) {
						dialogview = getDialog();
						dialogview = getDialog();
						et_title = (EditText) dialogview.findViewById(R.id.et_setgoal_title);
						et_amount = (EditText) dialogview.findViewById(R.id.et_setgoal_amount);
						title = et_title.getText().toString();
						amount = et_amount.getText().toString();
						if (title.length() > 0 && amount.length() > 0 ) {
							controller.saveGoal(title, amount);
							Fragment fragment = getFragmentManager().findFragmentById(2131296256);
							fragment.onActivityResult(1002, 1, null);
							dismiss();
						}else {
							Toast.makeText(context, getString(R.string.invalid_goal), Toast.LENGTH_SHORT).show();
						}
						
					}
				});
		return dialog;
	}
}
