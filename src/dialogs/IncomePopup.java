package dialogs;

import java.util.Calendar;

import se.tuxflux.assignment.one.R;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.widget.EditText;
import android.widget.Toast;
import controllers.IncomeController;
import controllers.IncomePopupController;

/**
 * Creates a dialog where users are allowed add an income.
 * 
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 * 
 */
public class IncomePopup extends DialogFragment {

	private EditText et_title, et_category, et_amount;
	private String title, category, amount;
	private Dialog dialogview;
	private IncomePopupController controller;
	private IncomeController parentControler;
	private Fragment parent;
	private Calendar cal = Calendar.getInstance();

	@Override
	public Dialog onCreateDialog(Bundle savedInstanceState) {

		AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
		LayoutInflater inflater = getActivity().getLayoutInflater();
		dialogview = getDialog();
		controller = new IncomePopupController();
		parentControler = new IncomeController(getParent());
		builder.setView(inflater.inflate(R.layout.dialog_addincome, null))

		.setPositiveButton(getString(R.string.add_income),
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialogview = getDialog();
						et_title = (EditText) dialogview
								.findViewById(R.id.et_income_title);
						et_category = (EditText) dialogview
								.findViewById(R.id.et_income_category);
						et_amount = (EditText) dialogview
								.findViewById(R.id.et_income_amount);
						title = et_title.getText().toString();
						category = et_category.getText().toString();
						amount = et_amount.getText().toString();
						long timestamp = cal.getTimeInMillis();
						controller.addIncome(title, category, Double.valueOf(amount), timestamp);
						Fragment fragment = getFragmentManager().findFragmentById(2131296256);
						fragment.onActivityResult(2001, 1, null);
					}
				});
		return builder.create();
	}

	public Fragment getParent() {
		return parent;
	}

	public void setParent(Fragment parent) {
		this.parent = parent;
	}
}
