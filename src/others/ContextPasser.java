package others;

import android.content.Context;

/**
 * A clever class that passes the MainActivity context!/**
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 */

public class ContextPasser {
	private static Context localContext;
	
	/**
	 * Returns context
	 * 
	 * @return context. Use setLocalContext to set context in MainActivity.
	 */
	public static Context getLocalContext() {
		return localContext;
	}

	/**
	 * Sets the context to be passed.
	 * 
	 * @param localContext - context that should be set, preferebly mainactivity.
	 */
	public static void setLocalContext(Context localContext) {
		ContextPasser.localContext = localContext;
	}

}
