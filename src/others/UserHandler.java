package others;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;

/**
 * Writes and reads user.
 * 
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 */

public class UserHandler {

	private Context context = ContextPasser.getLocalContext();
	private final String SP_USER = "sp_name";
	private final String KEY_FIRSTNAME = "key_firstname";
	private final String KEY_SURNAME = "key_surname";
	private final String KEY_CURRENCY = "key_currency";
	private final String KEY_NEW_USER = "key_newuser";
	private static String staticCurrency;

	/**
	 * Saves user.
	 * @param firstname - first name of user.
	 * @param surname - surname of user.
	 * @param currency - prefered currency.
	 * @return true if save was successful.
	 */
	public boolean saveUser(String firstname, String surname, String currency) {
		SharedPreferences sp = context.getSharedPreferences(SP_USER,
				Context.MODE_PRIVATE);
		Editor editor = sp.edit();

		editor.putString(KEY_FIRSTNAME, firstname);
		editor.putString(KEY_SURNAME, surname);
		editor.putString(KEY_CURRENCY, currency);
		editor.putBoolean(KEY_NEW_USER, false);
		return editor.commit();
	}

	/**
	 * Returns name of user.
	 * @return name of user.
	 */
	public String getName() {
		SharedPreferences sp = context.getSharedPreferences(SP_USER,
				Context.MODE_PRIVATE);
		return sp.getString(KEY_FIRSTNAME, "");
	}

	/**
	 * Returns surname of user.
	 * @return surname of user.
	 */
	public String getSurname() {
		SharedPreferences sp = context.getSharedPreferences(SP_USER,
				Context.MODE_PRIVATE);
		return sp.getString(KEY_SURNAME, "");
	}

	/**
	 * Return full name of user (name and surname).
	 * @return full name of user.
	 */
	public String getFullName() {
		String fullName;
		SharedPreferences sp = context.getSharedPreferences(SP_USER,
				Context.MODE_PRIVATE);
		fullName = sp.getString(KEY_FIRSTNAME, "");
		fullName += " " + sp.getString(KEY_SURNAME, "");
		return fullName;
	}

	/**
	 * Returns users prefered currency.
	 * @return user prefered currency.
	 */
	public String getCurrency() {
		SharedPreferences sp = context.getSharedPreferences(SP_USER,
				Context.MODE_PRIVATE);
		return sp.getString(KEY_CURRENCY, "");
	}

	/**
	 * Checks if user has submitted info.
	 * @return true if user exists, else false.
	 */
	public boolean newUser() {
		SharedPreferences sp = context.getSharedPreferences(SP_USER,
				Context.MODE_PRIVATE);
		return sp.getBoolean(KEY_NEW_USER, true);
	}

	/**
	 * Returns a static String, representing the users prefered currency.
	 * @return users prefered currency. 
	 */
	public static String getStaticCurrency() {
		return staticCurrency;
	}
	
	/**
	 * Sets a static String, representing the users prefered currency.
	 */
	public static void setStaticCurrency(String currency) {
		staticCurrency = currency;
	}
}
