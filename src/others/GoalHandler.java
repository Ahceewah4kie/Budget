package others;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.util.Log;

/**
 * Writes and reads goals.
 * 
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 */

public class GoalHandler {
	private Context context = ContextPasser.getLocalContext();
	private static String GOAL_SHARED_PREFERENCE = "sp_goal_key";
	private static String TITLE_KEY = "goal_title_key";
	private static String AMOUNT_KEY = "goal_amount_key";

	/**
	 * Saves a goal.
	 * 
	 * @param title
	 *            - goal title.
	 * @param amount
	 *            - goal amount.
	 */
	public void saveGoal(String title, String amount) {
		saveGoalTitle(title);
		saveGoalAmount(amount);
	}

	/**
	 * Saves title and returns if it was successful.
	 * 
	 * @param title
	 *            - goal title.
	 * @return true if save was successful.
	 */
	public boolean saveGoalTitle(String title) {
		SharedPreferences sp = context.getSharedPreferences(
				GOAL_SHARED_PREFERENCE, Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putString(TITLE_KEY, title);
		boolean success = editor.commit();
		Log.i(TITLE_KEY, "title=" + title + ", success=" + success);
		return success;
	}

	/**
	 * Saves amount and returns true if successful.
	 * 
	 * @param amount - goal amount.
	 * @return true if amount was saves successfully.
	 */
	public boolean saveGoalAmount(String amount) {
		SharedPreferences sp = context.getSharedPreferences(
				GOAL_SHARED_PREFERENCE, Context.MODE_PRIVATE);
		Editor editor = sp.edit();
		editor.putString(AMOUNT_KEY, amount);
		boolean success = editor.commit();
		Log.i(AMOUNT_KEY, "amount=" + amount + ", success=" + success);
		return success;
	}

	/**
	 * Gets goal title.
	 * @return title of goal.
	 */
	public String getGoalTitle() {
		SharedPreferences sp = context.getSharedPreferences(
				GOAL_SHARED_PREFERENCE, Context.MODE_PRIVATE);
		String title = sp.getString(TITLE_KEY, "");
		Log.i(AMOUNT_KEY, "title=" + title);
		return title;
	}

	/**
	 * Gets goal amount.
	 * @return amount of goal.
	 */
	public String getGoalAmount() {
		SharedPreferences sp = context.getSharedPreferences(
				GOAL_SHARED_PREFERENCE, Context.MODE_PRIVATE);
		String title = sp.getString(AMOUNT_KEY, "");
		Log.i(AMOUNT_KEY, "title=" + title);
		return title;
	}
}
