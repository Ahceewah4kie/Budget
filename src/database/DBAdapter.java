package database;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

/**
 * A database class. See static strings for info about tables.
 * 
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 * 
 */
public class DBAdapter {

	private final String INCOME_TAG = "Budget | DBAdapter | Income";
	private final String EXPENSE_TAG = "Budget | DBAdapter | Expense";
	private final String CREATE_Expense = "CREATE TABLE expense(title TEXT, category TEXT, price DOUBLE, picture BLOB, id INTEGER PRIMARY KEY AUTOINCREMENT, timestamp INTEGER, ean TEXT);"; // ,
	private final String CREATE_INCOME = "CREATE TABLE income(title TEXT, category TEXT, amount DOUBLE, id INTEGER PRIMARY KEY AUTOINCREMENT, timestamp INTEGER);"; // timestamp
																																											// TIMESTAMP
	private SQLiteDatabase database;
	private DatabaseHelper helper;

	public DBAdapter(Context context) {
		helper = new DatabaseHelper(context);
	}

	/**
	 * Method used to write rows to the expense table. Don't forget to call
	 * open() before calling this method.
	 * 
	 * @param title
	 *            - title of expense
	 * @param category
	 *            - category of expense
	 * @param price
	 *            - price of expense
	 * @param picture
	 *            - picture of expense
	 * @param ID
	 *            - ID of expense (Pri. key)
	 */
	public void addExpense(String title, String category, double price,
			byte[] picture, long timestamp, String ean) {
		ContentValues vals = new ContentValues();
		vals.put("title", title);
		vals.put("category", category);
		vals.put("price", price);
		vals.put("picture", picture);
		StringBuffer s = new StringBuffer();
		for (int i = 0; i < picture.length; i++) {
			s.append(picture.toString());
		}
		Log.i(EXPENSE_TAG, "picture=" + s.toString());
		vals.put("timestamp", timestamp);
		vals.put("ean", ean);
		database.insert("expense", null, vals);
		Log.i(EXPENSE_TAG, "Added: " + vals);
	}

	/**
	 * Method used to write rows to the expense table. Don't forget to call
	 * open() before calling this method.
	 * 
	 * @param title
	 *            - title of expense
	 * @param category
	 *            - category of expense
	 * @param price
	 *            - price of expense
	 * @param picture
	 *            - picture of expense
	 * @param ID
	 *            - ID of expense (Pri. key)
	 */
	public void addExpense(String title, String category, double price,
			long timestamp, String ean) {
		ContentValues vals = new ContentValues();
		vals.put("title", title);
		vals.put("category", category);
		vals.put("price", price);
		vals.put("timestamp", timestamp);
		vals.put("ean", ean);
		database.insert("expense", null, vals);
		Log.i(EXPENSE_TAG, "Added: " + vals);
	}

	/**
	 * Method used to write rows to the income table. Don't forget to call
	 * open() before calling this method.
	 * 
	 * @param title
	 *            - title of income
	 * @param category
	 *            - category of income
	 * @param price
	 *            - price of income
	 * @param ID
	 *            - ID of income (Pri. key)
	 */
	public void addIncome(String title, String category, double amount,
			long timestamp) {
		ContentValues vals = new ContentValues();
		vals.put("title", title);
		vals.put("category", category);
		vals.put("amount", amount);
		vals.put("timestamp", timestamp);
		database.insert("income", null, vals);
		Log.i(INCOME_TAG, "Added: " + vals);
	}
	
	/**
	 * Method used to update rows to the income table. Don't forget to call
	 * open() before calling this method.
	 * 
	 * @param title
	 *            - title of income
	 * @param category
	 *            - category of income
	 * @param price
	 *            - price of income
	 * @param ID
	 *            - ID of income (Pri. key)
	 */
	public void editIncome(String title, String category, double amount, int id) {
		ContentValues vals = new ContentValues();
		vals.put("title", title);
		vals.put("category", category);
		vals.put("amount", amount);
		database.update("income", vals, "id="+id, null);
	}

	/**
	 * Removes all incomes with the same id as parameter.
	 * @param id - the id of incomes to be removed.
	 */
	public void removeIncome(int id) {
		database.delete("income", "id = "+id, null);
	}

	/**
	 * Gets a cursor "SELECT SUM(price) FROM expense".
	 * @return a cursor of the query "SELECT SUM(price) FROM expense".
	 */
	public Cursor getExpenseSumCursor() {
		Cursor c = database.rawQuery("SELECT SUM(price) FROM expense", null);
		return c;
	}	
	
	/**
	 * Gets a cursor "SELECT SUM(price) FROM income".
	 * @return a cursor of the query "SELECT SUM(price) FROM income".
	 */
	public Cursor getIncomeSumCursor() {
		Cursor c = database.rawQuery("SELECT SUM(amount) FROM income", null);
		return c;
	}
	
	/**
	 * Method used to update rows to the expense table. Don't forget to call
	 * open() before calling this method.
	 * 
	 * @param title
	 *            - title of expense
	 * @param category
	 *            - category of expense
	 * @param price
	 *            - price of expense
	 * @param ID
	 *            - ID of expense (Pri. key)
	 */
	public void editExpense(String title, String category, double amount, int id) {
		ContentValues vals = new ContentValues();
		vals.put("title", title);
		vals.put("category", category);
		vals.put("price", amount);
		database.update("expense", vals, "id="+id, null);
	}

	/**
	 * Method used to get values from the expense table. Will send a
	 * SELECT-query that gets all values from that table.
	 * 
	 * @return a cursor, pointing at a row in the expense table.
	 */
	public Cursor getExpense(long startdate, long enddate) {
		Cursor c = database.rawQuery(
				"SELECT * FROM expense WHERE timestamp >= " + startdate
						+ " AND timestamp <= " + enddate, null);
		return c;
	}
	
	/**
	 * Method used to get values from the expense table. Will send a
	 * SELECT-query that gets all values from that table.
	 * 
	 * @return a cursor, pointing at a row in the expense table.
	 */
	public Cursor getLatestExpense() {
		Cursor c = database.rawQuery(
				"SELECT * FROM expense ORDER BY id DESC LIMIT 1", null);
		return c;
	}

	/**
	 * Method used to get values from the expense table. Will send a
	 * SELECT-query that gets all values from that table.
	 * 
	 * @return a cursor, pointing at a row in the expense table.
	 */
	public Cursor getAllExpenses() {
		Cursor c = database.rawQuery("SELECT * FROM expense", null);
		return c;
	}
	
	/**
	 * Removes all expenses with the same id as parameter.
	 * @param id - the id of expenses to be removed.
	 */
	public void removeExpense(int id) {
		database.delete("expense", "id = "+id, null);		
	}
	
	/**
	 * Method used to get values from the income table. Will send a SELECT-query
	 * that gets all values from that table.
	 * 
	 * @return a cursor, pointing at a row in the income table.
	 */
	public Cursor getIncome(long startdate, long enddate) {
		Cursor c = database.rawQuery("SELECT * FROM income WHERE timestamp >= "
				+ startdate + " AND timestamp <= " + enddate, null);
		return c;
	}
	
	/**
	 * Gets a cursor to the last added income.
	 * @returna cursor to the last added income.
	 */
	public Cursor getLatestIncome() {
		Cursor c = database.rawQuery(
				"SELECT * FROM income ORDER BY id DESC LIMIT 1", null);
		return c;
	}
	
	/**
	 * Gets a cursor to all incomes in income.
	 * @return a cursor to all incomes in income.
	 */
	public Cursor getAllIncome() {
		Cursor c = database.rawQuery("SELECT * FROM income", null);
		return c;
	}

	/**
	 * Gets a cursor to all expenses with the ean in parameter.
	 * @param ean - the ean to query after.
	 * @return a cursor to all expenses with the ean in parameter.
	 */
	public Cursor getEan(String ean) {
		Cursor c = database.rawQuery("SELECT * FROM expense WHERE ean == "+ean, null);
		return c;
	}
	/**
	 * Method called to open the database. Call this method before any other.
	 * 
	 * @return a DBAdapter for this database.
	 */
	public DBAdapter open() {
		database = helper.getWritableDatabase();
		return this;
	}

	/**
	 * Method called to close the database. Call this method when you're done.
	 */
	public void close() {
		database.close();
	}

	private class DatabaseHelper extends SQLiteOpenHelper {

		private static final String NAME = "myDatabase";
		private static final int VERSION = 2;

		public DatabaseHelper(Context context) {
			super(context, NAME, null, VERSION);
		}

		@Override
		public void onCreate(SQLiteDatabase db) {
			db.execSQL(CREATE_Expense);
			db.execSQL(CREATE_INCOME);
		}

		@Override
		public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		}
	}

	
}