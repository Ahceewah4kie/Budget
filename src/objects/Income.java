package objects;


/**
 * An income object.
 * @author Simon Cedergren AB1539 <simon@tuxflux.se>
 * 
 */
public class Income {
	private String title;
	private String category;
	private Double amount;
	private int id;
	private long timestamp;

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the category
	 */
	public String getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(String category) {
		this.category = category;
	}

	/**
	 * @return the price
	 */
	public Double getAmount() {
		return amount;
	}

	/**
	 * @param amount the price to set
	 */
	public void setAmount(Double amount) {
		this.amount = amount;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the timestamp
	 */
	public long getTimestamp() {
		return timestamp;
	}

	/**
	 * @param timestamp the timestamp to set
	 */
	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	@Override
	public String toString() {
		return "Expence [title=" + title + ", category=" + category + ", price=" + amount + ", id=" + id + ", timestamp=" + timestamp + "]";
	}

}